# Gateway DNA Tutorial

A crash-course in lightweight science gateways.


## Outline

1. Let's all do the iPlant!
1. Brief introduction to client-side development
    1. How does this apply to science gateways?
1. Gateway DNA: Boilerplate application
1. Hands on:
    1. Authentication
    1. Application listing
    1. IO
    1. Application execution
    1. Sharing data
1. Tidbits
    1. Free hosting! Google. Dropbox.
1. Embedding/Widgets


## URLs

The following URLs are important:

- [https://bitbucket.org/taccaci/foundation-backbone](https://bitbucket.org/taccaci/foundation-backbone)
- [https://bitbucket.org/taccaci/gateway-dna-tutorial](https://bitbucket.org/taccaci/gateway-dna-tutorial)


## Get an iPlant Account


### [Register for an iPlant account](https://user.iplantcollaborative.org).

The iPlant Collaborative…

An iPlant account gives you full access to use all of the computational infrastructure that iPlant offers.  This tutorial focused on using the Agave API for building lightweight science gateways.


## Client-side application development

- [Backbone.js](http://backbonejs.org)
- [TodoMVC](http://todomvc.com/)


## Gateway DNA: Boilerplate

- [https://bitbucket.org/taccaci/foundation-backbone](https://bitbucket.org/taccaci/foundation-backbone)
- [http://iplant-dev.tacc.utexas.edu/v1/foundation-backbone](http://iplant-dev.tacc.utexas.edu/v1/foundation-backbone/)


### But wait!  There's more!

How would you like to host this for free? If you could choose any infrastructure to host on, what would it be?  [The ultimate web host?](http://bit.ly/192iUKm)

You can host Gateway DNA applications on any infrastructure, from a local file:// or a basic webserver to Google Drive, Dropbox, or CDNs.


## Hands on

Some notes on the examples.  If you are running these from your local filesystem,
these examples will work best in Firefox.  Google Chrome has security restrictions
that prevent dynamic loading of assets.  This, however, is not a problem production,
it's just a "bug" in Chrome with how it handles the file:// protocol.

### Example 1: Authentication

Agave Auth: backbone-agave.js

This example will show how to set up authentication using the Backbone/Agave plugins.

1. Open index.html in Firefox
2. Edit scripts/app.js
3. Edit scripts/views/login-view.js

### Example 2: Application Listing

Agave Apps: backbone-agave-apps.js

This example shows a little more detail in dealing with events
and combining views to create more complex interfaces.

1. Open index.html in Firefox
2. Edit scripts/app.js
3. Edit scripts/views/applist-view.js
4. Taking a look at templates

### Example 3: Data Store

Agave IO: backbone-agave-io.js

This example demonstrates more event handling and even more complex
interfaces.

1. Open index.html in Firefox
3. View scripts/views/file-browser-view.js
3. View scripts/views/file-browser-list-view.js
3. View scripts/views/file-browser-list-item-view.js
4. Events, more complicated rendering

### Example 4: Application Execution

Agave Job submission: backbone-agave-jobs.js

This example demonstrates custom event handling, complex views,
and view reuse.

1. Open index.html in Firefox
2. View app-view.js
3.

### Example 5: Sharing/Collaboration with Post-Its

Agave PostIts: backbone-agave-postit.js

This example demonstrates view reuse.

1. Open index.html in Firefox
2. View scripts/views/postit-form-view.js

## Bonus Tidbits!

### Get Free Hosting!

## Embedding/Widgets