require.config({
    paths: {
        jquery: '../bower_components/jquery/jquery',
        underscore: '../bower_components/underscore/underscore',
        backbone: '../bower_components/backbone/backbone',
        handlebars: '../bower_components/handlebars/handlebars',
        text: '../bower_components/requirejs-text/text',
        bootstrap: 'vendor/bootstrap',
        agave: '../bower_components/backbone-agave/backbone-agave',
        templates: '../templates'
    },
    shim: {
        bootstrap: {
            deps: ['jquery'],
            exports: 'jquery'
        },
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ['underscore'],
            exports: 'Backbone'
        },
        handlebars: {
            exports: 'Handlebars'
        },
        agave: {
            deps: ['backbone'],
            exports: 'Backbone'
        }
    }
});

require([
        'handlebars',
        'app',
        'views/login-view',
    ],
    function (handlebars, app) {
        'use strict';
        app.init();
    }
);
