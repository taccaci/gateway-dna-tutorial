/*global define */
define([], function () {
    'use strict';
    var App = {
      Layouts: {},
      Views: {},
      Models: {},
      Collections: {},
      Routers: {},

      init: function() {
        // App.Agave = new Backbone.Agave();
        // new App.Views.LoginView({el: '#login'}).render();
      }
    };
    return App;
});