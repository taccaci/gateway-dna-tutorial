/*global define*/
define(
  [
    'app',
    'backbone',
    'handlebars',
    'underscore',
    'jquery',
    'text!templates/login.html',
    'text!templates/logout.html',
    'agave'
  ],
  function(App, Backbone, Handlebars, _, $, loggedOutTemplate, loggedInTemplate) {
    var LoginView = Backbone.View.extend({

      loggedOutTemplate: Handlebars.compile(loggedOutTemplate),

      loggedInTemplate: Handlebars.compile(loggedInTemplate),

      render: function () {
        var templ;
        if (App.Agave.token().isValid()) {
          templ = this.loggedInTemplate;
        } else {
          templ = this.loggedOutTemplate;
        }
        this.$el.html(templ(App.Agave.token().toJSON()));
      },

      events: {
        'click .login-btn': 'doLogin',
        'click .logout-btn': 'doLogout'
      },

      doLogin: function(e) {
        e.preventDefault();
        window.alert('TODO: Log in!');

        // var view = this;
        // if (! App.Agave.token().save({
        //   username: $('input[name=username]').val()
        // }, {
        //   password: $('input[name=password]').val(),
        //   success: function() {
        //     view.render();
        //   },
        //   error: function() {
        //     window.alert('Login failed!  Please try again.');
        //   }
        // })) {
        //   window.alert('Login failed!  Please try again.');
        // }

        return false;
      },

      doLogout: function(e) {
        e.preventDefault();
        window.alert('TODO: Log out!');

        var view = this;
        // App.Agave.token().destroy({
        //   success: function() {
        //     view.render();
        //   }
        // });

        return false;
      }
    });

    App.Views.LoginView = LoginView;
    return LoginView;
  }
);