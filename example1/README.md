# Gateway DNA Tutorial - Example 1

Authentication with Backbone Agave

## Outline

1. Basic structure of application
2. Basics of Backbone-Agave
3. Log in with iPlant

### Application structure

This application is build using the Asynchronous Module Definition (AMD) design pattern.
What this means is you don't need to make sure that all of your dependencies are included
in the correct order.  Instead, you declare your dependencies, and then the AMD loader
([RequireJS](http://requirejs.org/)) loads dependencies asynchronously, on an as-needed basis.

Check out app/scripts/main.js to get an idea of how this works.

So the main application runner is app/scripts/main.js.  This starts the app.  The main
application module is app/scripts/app.js.  The rest of the views are in app/scripts/views.

### Basics of Backbone-Agave

app/bower_components/backbone-agave/backbone-agave.js
app/bower_components/backbone-agave/backbone-agave-*.js

### Log in with iPlant

Take a look at app/scripts/views/login-view.js.

- anatomy of an AMD module
- templating
- basic event handling

#### Let's do it!

Edit app/scripts/app.js to enable the the login-view.

Uncomment the handlers in app/scripts/views/login-view.js#doLogin and app/scripts/views/login-view.js#doLogout.

Log in with iPlant!
