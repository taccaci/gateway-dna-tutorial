/*global define */
define(['underscore', 'backbone'], function (_, Backbone) {
    'use strict';
    var App = {
      Layouts: {},
      Views: {},
      Models: {},
      Collections: {},
      Routers: {},

      init: function() {
        App.Agave = new Backbone.Agave();
        new App.Views.LoginView({el: '#login'}).render();

        var applist, mainview = new App.Views.MainView({el: '#main'});
        mainview.render();

        // App.Agave.token().on('change', function() {
        //   if (App.Agave.token().isValid()) {
        //     applist = applist || new App.Views.ApplicationListView({el: '#main'});
        //     applist.render();
        //   } else {
        //     mainview.render();
        //   }
        // });
      }
    };
    return _.extend(App, Backbone.Events);
});