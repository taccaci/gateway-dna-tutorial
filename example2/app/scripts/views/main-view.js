/*global define*/
define(
  [
    'app',
    'backbone',
    'handlebars',
    'underscore',
    'jquery',
    'text!templates/main.html',
    'agave'
  ],
  function(App, Backbone, Handlebars, _, $, template) {

    var MainView = Backbone.View.extend({
      template: Handlebars.compile(template),

      render: function () {
        this.$el.html(this.template());
      }

    });

    App.Views.MainView = MainView;
    return MainView;
  }
);