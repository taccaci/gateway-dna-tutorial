/*global define*/
define(
  [
    'app',
    'backbone',
    'handlebars',
    'underscore',
    'jquery',
    'text!templates/applist-item.html',
    'agave',
    'agaveApps'
  ],
  function(App, Backbone, Handlebars, _, $, template) {

    var AppView = Backbone.View.extend({

      template: Handlebars.compile(template),

      render: function () {
        this.$el.html(this.template(this.model.toJSON()));
      }
    });

    App.Views.ApplicationView = AppView;
    return AppView;
  }
);