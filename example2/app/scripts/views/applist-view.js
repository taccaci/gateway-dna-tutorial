/*global define*/
define(
  [
    'app',
    'backbone',
    'handlebars',
    'underscore',
    'jquery',
    'text!templates/applist.html',
    'agave',
    'agaveApps'
  ],
  function(App, Backbone, Handlebars, _, $, template) {

    var AppListView = Backbone.View.extend({
      initialize: function() {
        this.collection = new Backbone.Agave.Apps.PublicApplications();
        this.listenTo(this.collection, 'reset add remove', this.render);
        this.collection.fetch({reset: true});
      },

      loading: true,

      render: function () {
        this.$el.html(Handlebars.compile(template)({
          loading: this.loading
        }));

        this.loading = false;

        // var appUl = this.$el.find('.applist');
        // this.collection.each(function(application) {
        //   var li = $('<li>');
        //   appUl.append(li);
        //   var listItem = new App.Views.ApplicationView({el: li, model: application});
        //   listItem.render();
        // }, this);
      },

      events: {
        'click .view-app': 'viewApp'
      },

      viewApp: function() {

      }
    });

    App.Views.ApplicationListView = AppListView;
    return AppListView;
  }
);