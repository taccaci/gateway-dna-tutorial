# Gateway DNA Tutorial - Example 2

Application Discovery with Backbone Agave

## Outline

1. Handling events on login
2. Combining views (KISS!)

## Handling login events.

Open app/scripts/app.js.  Uncomment the change event handler
for the Agave token that we get when we log in.

## Combining views

Open app/scripts/views/applist-view.js and app/scripts/views/app-view.js.
The applist-view renders the app-view inside of itself.  This helps us
to separate concerns and make views reusable.

Uncomment the view embedding code in applist-view.