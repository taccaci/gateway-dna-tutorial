# Gateway DNA Tutorial

## Outline

1. Prerequisites
2. Installation
3. First run
4. Customization
5. Testing
6. Deployment


## Prerequisites

Gateway DNA requires that Node.js with NPM be installed, as well as Ruby with the Sass and Compass rubygems.  You also need the Git Source Control Manager.

### Helpful tools

On MacOS X it is helpful to have the [Homebrew package manager installed](http://mxcl.github.io/homebrew/).  Many of the required libaries and packages are preinstalled on Mac OS X, but it is often easier to go ahead and re-install the packages using Homebrew.  This allows easy updating of the version of the package as well as configuration changes that do not affect the default OS-installed packages and also helps to preserve your installations and configurations across OS upgrades.

### Install Node.js

http://nodejs.org/

#### Windows

Download and run the installer from [this page](http://nodejs.org/).  Confirm your installation with the following steps:

1. Windows key + R
2. Type "CMD" and Enter
3. Type "node --version" and Enter

You should see output similar to:

    C:\Users\Matthew Hanlon>node --version
    v0.10.13

    C:\Users\Matthew Hanlon>

You have successfully installed Node.js.

#### MacOS X or Linux

On MacOS X or Linux you can either install via a package manager (homebrew, apt, yum)
or the download package from [this page](http://nodejs.org).

### Install Ruby

#### Windows

Download and run the installer [here](http://rubyinstaller.org/).  Confirm your installation with the following steps:

1. Windows key + R
2. Type "CMD" and Enter
3. Type "ruby -v" and Enter

You should see output similar to:

    C:\Users\Matthew Hanlon>ruby -v
    ruby 2.0.0p247 (2013-06-27) [x64-mingw32]

    C:\Users\Matthew Hanlon>

#### MacOS X

MacOS comes with Ruby preinstalled.  However, this installation is a root user installation and requires sudo
to install packages and make configuration changes.  Suggested is to install RVM.  http://rvm.io

#### Linux

You can install Ruby via your package manager, but as with MacOS X above, using RVM is easier.

### Install Git

#### Windows

You can install Git for Windows at this URL: [Git for Windows](http://msysgit.github.com/)

When installing you have to option to run Git from Git-bash only or update your PATH to run Git from the Windows Command Prompt.  I suggest the second choice, but either is OK.  If you choose to run Git from Git-bash only then you will need to run any Git commands from Git-bash instead of the standard Command Prompt.

#### MacOS X

Git may be preinstalled, but it is often useful to install Git using Homebrew to avoid
making changes to the default/globally installed Git that comes with the OS.  You can find more information [here](http://git-scm.com/book/en/Getting-Started-Installing-Git).

#### Linux

Install Git using your package manager.  You can find more information [here](http://git-scm.com/book/en/Getting-Started-Installing-Git).


### Install yeoman.io, grunt-cli, and bower

Yeoman, Grunt, and Bower are three awesome tools for Web Development, especially for today's modern, client-side Javascript applications.

We are going to install yeoman, grunt-cli, and bower using NPM.  Open a Command window and run the following command:

	C:\Users\Matthew Hanlon>npm install -g yo grunt-cli bower
	
This should install these three tools in the global scope.

### Install the Compass and Sass Rubygems

Compass and Sass are used for CSS3 styling.  Open a Command window and run the following command:

	C:\Users\Matthew Hanlon>gem install compass
	
Since Compass depends on Sass, it will be automatically installed with the Compass Rubygem.

## Congratulations! Now, the fun stuff…

Now that all prerequisties are installed we can get down to business.  Clone the GatewayDNA repository to your hard drive.

		C:\Users\Matthew Hanlon\workspace>git clone https://mrhanlon@bitbucket.org/taccaci/foundation-backbone.git
		C:\Users\Matthew Hanlon\workspace>cd foundation-backbone
		
This will checkout the GatewayDNA source to the directory `foundation-backbone`.  `CD` into the `foundation-backbone` directory and then run the following commands:

		C:\Users\Matthew Hanlon\workspace\foundation-backbone>npm install
		C:\Users\Matthew Hanlon\workspace\foundation-backbone>bower install
		
This will install all third-party extensions required by both the build+test+deploy tools as well as third-party libraries (e.g. javascript and css) that are used in the application.  You can view the dependencies and extensions that are used in the following files: `foundation-backbone\package.json` and `foundation-backbone\bower.json`.

## Run the server

Now that everything is installed, you can run the server with the following command:

	C:\workspace\foundation-backbone>grunt server
	
This will clean, test, and compile the application, start a Node.js server on [localhost:9000](http://localhost:9000), and open your default browser to that page.  **Congratulations!  You have successfully install the GatewayDNA Boilerplate application!**
	
	