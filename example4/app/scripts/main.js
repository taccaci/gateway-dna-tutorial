require.config({
    paths: {
        jquery: '../bower_components/jquery/jquery',
        underscore: '../bower_components/underscore/underscore',
        backbone: '../bower_components/backbone/backbone',
        handlebars: '../bower_components/handlebars/handlebars',
        text: '../bower_components/requirejs-text/text',
        bootstrap: 'vendor/bootstrap',
        agave: '../bower_components/backbone-agave/backbone-agave',
        agaveIo: '../bower_components/backbone-agave/backbone-agave-io',
        agaveApps: '../bower_components/backbone-agave/backbone-agave-apps',
        agaveJobs: '../bower_components/backbone-agave/backbone-agave-jobs',
        fileSaver: '../bower_components/file-saver/FileSaver',
        templates: '../templates'
    },
    shim: {
        bootstrap: {
            deps: ['jquery'],
            exports: 'jquery'
        },
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ['underscore'],
            exports: 'Backbone'
        },
        handlebars: {
            exports: 'Handlebars'
        },
        fileSaver: {
            exports: 'fileSaver'
        },
        agave: {
            deps: ['backbone'],
            exports: 'Backbone'
        },
        agaveIo: {
            deps: ['agave'],
            exports: 'Backbone'
        },
        agaveApps: {
            deps: ['agave', 'agaveIo'],
            exports: 'Backbone'
        },
        agaveJobs: {
            deps: ['agave'],
            exports: 'Backbone'
        }
    }
});

require([
        'app',
        'views/main-view',
        'views/login-view',
        'views/app-view',
        'views/file-browser-list-view',
        'views/file-browser-list-item-view'
    ],
    function (app) {
        'use strict';
        app.init();
    }
);
