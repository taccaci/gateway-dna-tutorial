/*global define*/
define(
  [
    'app',
    'backbone',
    'handlebars',
    'underscore',
    'jquery',
    'text!templates/app-form.html',
    'agave',
    'agaveApps',
    'agaveJobs',
    'bootstrap'
  ],
  function(App, Backbone, Handlebars, _, $, formTemplate) {

    var AppView = Backbone.View.extend({

      formTemplate: Handlebars.compile(formTemplate),

      initialize: function() {
        this.listenTo(this.model, 'change', this.render);
      },

      render: function () {

        var parameters = [];

        _.each(
          _.sortBy(this.model.get('parameters'), function(p) { return p.id; }),
          function(param) {
            if (param.value.visible) {
              switch (param.value.type) {
              case 'bool':
                parameters.push({
                  isCheckbox: true,
                  id: param.id,
                  name: param.id,
                  label: param.details.label,
                  help: param.details.description,
                  defaultValue: param.defaultValue,
                  required: param.value.required
                });
                break;
              case 'string':
                var regexp = /^([^|]+[|])+[^|]+$/;
                if (regexp.test(param.value.validator)) {
                  var options = [];
                  _.each(param.value.validator.split('|'), function(opt) {
                    options.push({
                      value: opt,
                      label: opt,
                      selected: opt === param.defaultValue
                    });
                  });
                  parameters.push({
                    isSelect: true,
                    id: param.id,
                    name: param.id,
                    label: param.details.label,
                    help: param.details.description,
                    options: options,
                    required: param.value.required
                  });
                  break;
                } else if (regexp.test(param.details.description)) {
                  var options = [];
                  _.each(param.details.description.split('|'), function(opt) {
                    options.push({
                      value: opt,
                      label: opt,
                      selected: opt === param.defaultValue
                    });
                  });
                  parameters.push({
                    isSelect: true,
                    id: param.id,
                    name: param.id,
                    label: param.details.label,
                    options: options,
                    required: param.value.required
                  });
                  break;
                } else {
                  // fall through to default
                }

              default:
                parameters.push({
                  isText: true,
                  id: param.id,
                  name: param.id,
                  label: param.details.label,
                  help: param.details.description,
                  defaultValue: param.defaultValue,
                  required: param.value.required
                });
              }
            }
          }
        );

        var inputs = _.sortBy(this.model.get('inputs'), function(file) { return file.id; });

        var json = {
          app: this.model.toJSON(),
          params: parameters,
          inputs: inputs
        };
        this.$el.html(this.formTemplate(json));

        this.fileChooser = new App.Views.FileBrowserListView({
          el: '.file-chooser .modal-body',
          collection: new Backbone.Agave.IO.Listing([], {
            path: App.Agave.token().get('username')
          })
        });
        this.fileChooser.collection.fetch({reset: true});
        this.fileChooser.render();
      },

      events: {
        'click .btn-choose-file': 'showFileChooser',
        'click .run-app': 'runApp'
      },

      showFileChooser: function(e) {
        e.preventDefault();
        var input = $(e.currentTarget).closest('.controls').find('input').attr('name');
        $('.file-chooser').modal();
        this.listenTo(this.fileChooser.collection, 'selected', function(selectedFile) {
          $('input[name=' + input + ']').val(selectedFile.get('path'));
          $('.file-chooser').modal('hide');
        });
        return false;
      },

      runApp: function(e) {
        e.preventDefault();

        var view = this;

        var formValues = this.$el.find('form').serializeArray(),
          jobData = {};

        _.each(formValues, function(fv) {
          jobData[fv.name] = fv.value;
        });

        jobData.archive = true;
        jobData.archivePath = '/' + App.Agave.token().get('username') + '/' + jobData.jobName;

        var jobSubmission = new Backbone.Agave.Jobs.Job();
        if (! jobSubmission.save(jobData, {
          success: function(model, response) {
            window.alert('Success!  Your job has been submitted, id: ' + model.id + '.\n\nThe output will be archived in your iPlant Data Store at: ' + model.get('archivePath'));
            view.render();
          },
          error: function() {
            window.alert('An unexpected error occurred while submitting your job.  Please try again!')
          }
        })) {
          window.alert('There was an error submitting your job.  Please check your form and try again!')
        }

        return false;
      }
    });

    App.Views.ApplicationView = AppView;
    return AppView;
  }
);