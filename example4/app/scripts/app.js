/*global define */
define(['underscore', 'backbone'], function (_, Backbone) {
    'use strict';
    var App = {
      Layouts: {},
      Views: {},
      Models: {},
      Collections: {},
      Routers: {},

      init: function() {
        App.Agave = new Backbone.Agave();
        new App.Views.LoginView({el: '#login'}).render();

        var view, mainview = new App.Views.MainView({el: '#main'});
        mainview.render();

        App.Agave.token().on('change', function() {
          if (App.Agave.token().isValid()) {
            view = view || new App.Views.ApplicationView({
              el: '#main',
              model: new Backbone.Agave.Apps.Application({id: 'head-stampede-5.97u2'})
            });
            view.model.fetch();
          } else {
            mainview.render();
          }
        });
      }
    };
    return _.extend(App, Backbone.Events);
});