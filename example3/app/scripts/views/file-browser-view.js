/*global define*/
define([
    'handlebars',
    'jquery',
    'underscore',
    'backbone',
    'app',
    'text!templates/file-browser.html',
    'agaveIo'
  ],
  function(Handlebars, $, _, Backbone, App, template) {
    var FileBrowser = Backbone.View.extend({

      template: Handlebars.compile(template),

      initialize: function() {
        this.collection = new Backbone.Agave.IO.Listing([], {
          path: App.Agave.token().get('username')
        });
        this.listenTo(this.collection, 'reset add remove', this.render);
        this.collection.fetch({reset: true});
      },

      render: function() {
        var parts = this.collection.path.split('/'),
          current = parts.pop();
        this.$el.html(this.template({
          parts: parts,
          current: current
        }));

        // file listing
        var listing = new App.Views.FileBrowserListView({
          el: this.$el.find('.browser'),
          collection: this.collection
        });
        listing.render();
      }

    });

    App.Views.FileBrowserView = FileBrowser;
    return FileBrowser;
  }
);