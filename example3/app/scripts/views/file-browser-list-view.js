/*global define*/
define([
    'handlebars',
    'jquery',
    'underscore',
    'backbone',
    'app',
    'text!templates/file-browser-list.html',
    'agaveIo'
  ],
  function(Handlebars, $, _, Backbone, App, template) {
    var FileBrowserList = Backbone.View.extend({

      template: Handlebars.compile(template),

      hasRendered: false,

      render: function() {
        if (this.collection.length === 0) {
          if (this.hasRendered) {
            this.$el.html('<div class="alert alert-info"><i class="icon icon-info-sign"></i> You don\'t have any files to display.</div>');
          } else {
            this.$el.html('<div class="alert alert-info"><h4>Loading your files...</h4> Please wait...</div>');
          }
        } else {
          this.$el.html(this.template());
          var list = this.$el.find('.file-list');
          this.collection.each(function(item) {
            var el = $('<li>');
            list.append(el);
            var itemView = new App.Views.FileBrowserListItemView({
              el: el,
              model: item
            });
            itemView.render();
          });
        }
        this.hasRendered = true;
      }

    });

    App.Views.FileBrowserListView = FileBrowserList;
    return FileBrowserList;
  }
);