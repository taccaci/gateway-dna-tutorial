/*global define */
define([], function (MainView) {
    'use strict';
    var App = {
      Layouts: {},
      Views: {},
      Models: {},
      Collections: {},
      Routers: {},

      init: function() {
        App.Agave = new Backbone.Agave();


        var login = new App.Views.LoginView({el: '#login'});
        login.render();

        var browser, main = new App.Views.MainView({el: '#main'});
        main.render();

        App.Agave.token().on('change', function() {
          if (App.Agave.token().isValid()) {
            browser = browser || new App.Views.FileBrowserView({el: '#main'});
            browser.render();
          } else {
            main.render();
          }
        });
      }
    };
    return App;
});