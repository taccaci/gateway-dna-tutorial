/*global define */
define([], function (MainView) {
    'use strict';
    var App = {
      Layouts: {},
      Views: {},
      Models: {},
      Collections: {},
      Routers: {},

      init: function() {
        App.Agave = new Backbone.Agave();


        var login = new App.Views.LoginView({el: '#login'});
        login.render();

        var view, main = new App.Views.MainView({el: '#main'});
        main.render();

        App.Agave.token().on('change', function() {
          if (App.Agave.token().isValid()) {
            view = view || new App.Views.PostItListView({
              el: '#main',
              collection: new Backbone.Agave.PostIt.ActivePostIts()
            });
            view.collection.fetch({reset: true});
          } else {
            main.render();
          }
        });
      }
    };
    return App;
});