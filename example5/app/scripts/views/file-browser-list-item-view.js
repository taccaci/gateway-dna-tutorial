/*global define*/
define([
    'handlebars',
    'jquery',
    'underscore',
    'backbone',
    'app',
    'text!templates/file-browser-list-item.html',
    'agaveIo'
  ],
  function(Handlebars, $, _, Backbone, App, template) {
    var FileItemView = Backbone.View.extend({
      template: Handlebars.compile(template),

      render: function() {
        var json = this.model.toJSON();

        var label = 'bytes', divisor = 1024, kb = 1024, mb = kb * 1024, gb = mb * 1024;
        if (json.length > gb) {
          divisor = gb;
          label = 'GB';
        } else if (json.length > mb) {
          divisor = mb;
          label = 'MB';
        } else if (json.length > kb) {
          divisor = kb;
          label = 'KB';
        }
        json.fileSize = json.length / divisor;
        json.fileSize = parseFloat(json.fileSize.toFixed(2));
        json.fileSizeLabel = label;

        if (json.type === 'dir') {
          json.icon = 'folder-close';
          json.isDir = true;
        } else {
          json.icon = 'file';
          json.isFile = true;
        }

        this.$el.html(this.template(json));
      },

      events: {
        'click .list-item': 'openOptions',
        'click .btn-open': 'openDir',
        'click .btn-select': 'selectFile'
      },

      openDir: function() {
        var path = this.model.get('path').slice(1);
        if (this.model.get('name') === '..') {
          path = path.slice(0, path.lastIndexOf('/'));
        }
        this.model.collection.path = path;
        this.model.collection.fetch({reset: true});
      },

      openOptions: function() {
        this.$el.find('.list-item-options').slideToggle();
      },

      selectFile: function(e) {
        this.model.collection.trigger('selected', this.model);
      }


    });

    App.Views.FileBrowserListItemView = FileItemView;
    return FileItemView;
  }
);