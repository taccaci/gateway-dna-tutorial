/*global define*/
define(
  [
    'app',
    'backbone',
    'handlebars',
    'underscore',
    'jquery',
    'text!templates/main.html',
    'agave'
  ],
  function(App, Backbone, Handlebars, _, $, template) {

    var MainView = Backbone.View.extend({
      template: Handlebars.compile(template),

      render: function () {
        this.$el.html(this.template());
      },

      events: {
        'click .login-btn': 'doLogin'
      },

      doLogin: function(e) {
        e.preventDefault();

        App.Agave = new Backbone.Agave();
        var token = App.Agave.token();
        if (token.save({
          username: $('input[name=username]').val()
        }, {
          password: $('input[name=password]').val()
        })) {

        } else {
          console.log(token.validationError);
        }

        return false;
      }

    });

    App.Views.MainView = MainView;
    return MainView;
  }
);