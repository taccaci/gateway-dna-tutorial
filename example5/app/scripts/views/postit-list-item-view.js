/*global define*/
define([
    'app',
    'backbone',
    'handlebars',
    'underscore',
    'jquery',
    'text!templates/postit-list-item.html',
    'agave',
    'agavePostIt'
  ],
  function(App, Backbone, Handlebars, _, $, template) {
  var PostItListItemView = Backbone.View.extend({
    template: Handlebars.compile(template),

    initialize: function() {
      this.listenTo(this.model, 'change', this.render);
    },

    render: function() {
      this.$el.html(this.template(this.model.toJSON()));
    },

    events: {
      'click .revoke-postit': 'revokePostit'
    },

    revokePostit: function(e) {
      e.preventDefault();

      if (window.confirm('Are you sure you want to revoke this PostIt?')) {
        this.model.destroy();
        this.remove();
      }

      return false;
    }
  });

  App.Views.PostItListItemView = PostItListItemView;
  return PostItListItemView;
});