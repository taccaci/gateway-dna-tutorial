/*global define*/
define([
    'app',
    'backbone',
    'handlebars',
    'underscore',
    'jquery',
    'text!templates/postit-list.html',
    'agave',
    'agavePostIt'
  ],
  function(App, Backbone, Handlebars, _, $, template) {
  var PostItListView = Backbone.View.extend({
    template: Handlebars.compile(template),

    initialize: function() {
      this.listenTo(this.collection, 'reset add remove', this.render);
    },

    render: function() {
      this.$el.html(this.template());

      var list = this.$el.find('.postit-list');
      this.collection.each(function(postit) {
        if (postit.get('remaining_uses') > 0) {
          var el = $('<div class="postit-view">');
          list.append(el);

          new App.Views.PostItListItemView({
            el: el,
            model: postit
          }).render();
        }
      });
      if (list.children().length === 0) {
        list.append('<div class="alert alert-info"><i class="icon icon-info-sign"></i> You don\'t have any Active PostIts!</div>');
      }
    },

    events: {
      'click .create-postit': 'showPostItForm'
    },

    showPostItForm: function(e) {
      e.preventDefault();

      var form = new App.Views.PostItFormView({
        el: this.el,
        model: new Backbone.Agave.PostIt.PostIt()
      });
      form.render();

      return false;
    }
  });

  App.Views.PostItListView = PostItListView;
  return PostItListView;
});