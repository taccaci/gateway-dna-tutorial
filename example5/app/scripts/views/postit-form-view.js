/*global define*/
define([
    'app',
    'backbone',
    'handlebars',
    'underscore',
    'jquery',
    'text!templates/postit-form.html',
    'agave',
    'agavePostIt',
    'bootstrap'
  ],
  function(App, Backbone, Handlebars, _, $, template) {
    var PostItForm = Backbone.View.extend({

      fileChooser: null,

      template: Handlebars.compile(template),

      render: function() {
        this.$el.html(this.template());

        this.fileChooser = new App.Views.FileBrowserListView({
          el: '.file-chooser .modal-body',
          collection: new Backbone.Agave.IO.Listing([], {
            path: App.Agave.token().get('username')
          })
        });
        this.fileChooser.collection.fetch({reset: true});
        this.fileChooser.render();
      },

      events: {
        'click .btn-choose-file': 'showFileChooser',
        'click .save-postit': 'savePostIt',
        'click .cancel-form': 'cancelForm'
      },

      showFileChooser: function(e) {
        e.preventDefault();
        var input = $(e.currentTarget).closest('.controls').find('input').attr('name');
        $('.file-chooser').modal();
        this.listenTo(this.fileChooser.collection, 'selected', function(selectedFile) {
          $('input[name=' + input + ']').val(selectedFile.downloadUrl());
          $('.file-chooser').modal('hide');
        });
        return false;
      },

      backToList: function() {
        var list = new App.Views.PostItListView({
          el: this.el,
          collection: new Backbone.Agave.PostIt.ActivePostIts()
        });
        list.collection.fetch({reset: true});
      },

      savePostIt: function(e) {
        e.preventDefault();

        var view = this, postData = {};

        var formValues = this.$el.find('form').serializeArray();
        _.each(formValues, function(val) {
          postData[val.name] = val.value;
        });

        if (! this.model.save(postData, {
          emulateJSON: true,
          data: postData,
          password: postData.password,
          success: function(model) {
            window.alert('PostIt created successfully!\n\n' + model.get('url'));
            view.backToList();
          },
          error: function() {
            window.alert('There was an unexpected error creating your PostIt.  Please try again.');
          }
        })) {
          window.alert('There was an error creating your PostIt.  Please check your submission and try again.');
        }

        return false;
      },

      cancelForm: function(e) {
        e.preventDefault();
        this.backToList();
        return false;
      }
    });

    App.Views.PostItFormView = PostItForm;
    return PostItForm;
  }
);