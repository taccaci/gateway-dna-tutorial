require.config({
    paths: {
        jquery: '../bower_components/jquery/jquery',
        underscore: '../bower_components/underscore/underscore',
        backbone: '../bower_components/backbone/backbone',
        handlebars: '../bower_components/handlebars/handlebars',
        text: '../bower_components/requirejs-text/text',
        bootstrap: 'vendor/bootstrap',
        agave: '../bower_components/backbone-agave/backbone-agave',
        agaveIo: '../bower_components/backbone-agave/backbone-agave-io',
        agavePostIt: '../bower_components/backbone-agave/backbone-agave-postit',
        templates: '../templates'
    },
    shim: {
        bootstrap: {
            deps: ['jquery'],
            exports: 'jquery'
        },
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ['underscore'],
            exports: 'Backbone'
        },
        handlebars: {
            exports: 'Handlebars'
        },
        agave: {
            deps: ['backbone'],
            exports: 'Backbone'
        },
        agaveIo: {
            deps: ['agave'],
            exports: 'Backbone'
        },
        agavePostIt: {
            deps: ['agave'],
            exports: 'Backbone'
        }
    }
});

require([
        'app',
        'views/main-view',
        'views/login-view',
        'views/postit-list-view',
        'views/postit-list-item-view',
        'views/postit-form-view',
        'views/file-browser-list-view',
        'views/file-browser-list-item-view'
    ],
    function (app) {
        'use strict';
        app.init();
    }
);
